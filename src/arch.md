# Architecture

## Prerequisites

The current application is built on LabVIEW 2019 Sp1 (the service pack is not important so much as the version year).
In addition to the packages supplied with the LabVIEW distribution, the main program uses the following packages that can be obtained through the VI Package Manager provided by JKI:

* SQLite Library from the NI LabVIEW ToolsNetwork provided by drjdpowell
* OpenG Toolkit provided by OpenG.org
* NI String Tools Library provided by National Instruments
* MGI Read/Write Anything prodvided by MGI

In addition to these libraries that can be found in the VIPM database, this program also uses some custom built packages
that will be described in more detail as far as the functionality below.  These are all provided by MSR Consulting, LLC
on request and are:

* General Reuse
* ConfigurableDevice
* DAQ Device

## Overview

The application consists of many VIs that appear to run independently but our connected via a global command pattern
that is handled in the main VI, `InstaScan ws`.  The tag `ws` is appended to the name to differentiate the current
implementation from previous implementations - namely that this application is web service based.  The initial
communication of the main application, originally called `InstaScan`, was via standard TCP/IP.  This approach itself
created a complicated and inefficient architecture which used resources unnecessarily and was difficult to debug.  The
current approach provides a simpler interface and separates concerns - requests are handled in web resources (namely GET
and POST http requests) and instrument operation is handled by a single VI, `Instascan ws`, that is driven by a queued
state machine.

The application itself is defined in a LabVIEW project file (`Instascan Proj.lvproj`) and contains a web service called
`instascope`, the main VI `InstaScan ws` and support VIs.  The application is built off of the web service, and in the
web service is a start up VI called `WebService Startup`.  This VI initializes the global command queue which is used to
send commands and requests from the user interface to the main application.  Once the command queue is initialized, the
main application is started.  This application runs until a request to stop the operation is received from the user
interface or a condition is encountered that requires the instrument to shutdown (such as low battery voltage).  The
final act of the application is to destroy the command queue and exit (although this is often not the case given that
the application is shutdown before then end of execution is reached). This VI is shown below.

![Web service startup](img/web-start.png)

The web server itself is not explicitly defined in the project.  It is provided by LabVIEW and requires no code within
the context of the project other than to define the web service itself. You can find more about how web services are
implemented by LabVIEW [here](https://www.ni.com/docs/en-US/bundle/labview/page/lvhowto/build_web_service.html).

