# Handling Events

Events are at the core of how user interactions are handled in the application.  The typical flow for handling user
interactions is as follows:

1. User enters request for action from the instrument via iOS
2. iOS sends an http request to the instrument which is handled by the web server
3. The web server enqueues a command that is sent to the main application 
4. The command is handled via a [command pattern](https://www.tutorialspoint.com/design_pattern/command_pattern.htm) implemented in the idle state of the application state machine

All implementations of commands can be found in the project directory `Classes/Commands`.  The commands are explained
below.

```uml
@startuml
abstract class "Command" as cmd
cmd : {abstract} Execute()
class "Stop Scan" as stopscan{
    Execute()
}
class "Start Scan" as startscan {
    Scan
    Scan ReadScan()
    WriteScan(Scan)
    Execute()
}

class "Start Session" as startsession{
    Session
    Session ReadSession()
    WriteSession(Session)
    Execute()
}
class "Stop Session" as stopsession {
    Execute()
}

class Scan {
    enum ScanType
    string RoomName
    string RoomNotes
    float RoomVolume
    int ScanSeqNo
    string ScanID
    string ScanItemID
    int Duration
    float StartTime
    float pctComplete
    float ScanTime
    float Countdown
    int MaxDuration
    string MetaID
    int MinScanTime

    void CalculateScanTime(bool)
    Scan DeserializeFromJson(string)
    string GetScanItemID()
    void IncrementScanID()
    bool PurgeDone(bool)
    void SetMaxScanDuration()
    void SetStartTimeToCurrentTime()
}

class "Session" as session {
    struct Customer
    string SessionID
    string VersionID
    float Level1
    float Level2
    float AvgData[][]
    float bgndMoldConc
    float BaselineSampleTime
    float B_G0Conc
    float B_G0AvgDia
    float B_G0F1AvgInt
    float B_G3Conc
    float B_G3AvgDia
    float B_G3F1AvgInt
    float B_G6Conc
    float B_G6AvgDia
    float B_G6F1AvgInt
    float B_M2F
    float B_M2T
    string Scanids[]
    bool Exists
    struct Inspector
    string Notes
    string ipad_version
}

class "Update Customer Data" as updatecust {
    struct CustomerData
    Execute()
}

class "Update iPad Version CMD" as updateipadver {
    string iPadVersion
    Execute()
}

class "Copy Configuration File CMD" as copycfg {
    string FileContents
    Excute()
}
startscan *-- Scan
startsession *-- session
cmd <|-- stopscan
cmd <|-- startscan
cmd <|-- startsession
cmd <|-- stopsession
cmd <|-- updatecust
cmd <|-- updateipadver
cmd <|-- copycfg

@enduml
```

## Start Session

## Stop Session

## Start Scan

## Stop Scan

## Copy Configuration File CMD

This commend takes the contents of the current configuration file and saves them to a archive file.  The archive file
can be found in the same directory as the current configuration file under the directory titled `Archive`.  The archived
configuration file will have the title `Config-<YYYYMMDD>.ini` where the bracketed value is the date the configuration
file is archived.  Once that file has been archived, the contents of the existing configuration file will be replaced
with the file contets specified by the command interface variable `FileContents`.  If there is an error, the web server
will send a response with the code and the source of the error.  The log file will show that this event was handled and
whether there was an error when handling the event.
